#include <cmath>
#include <iostream>
#include <vector>
#include <stdexcept>

/*
 * 0 for increasing array, 1 for increasing but for imperfect powers of 2,
 * 2 for decreasing array, 3 for decreasing but for imperfect powers of 2
 */
#define ARRAY_TYPE 0

// populates the vectors with numbers
std::vector<std::vector<int>> create_vectors()
{
    std::vector<std::vector<int>> arrays(12);

    if(ARRAY_TYPE == 0)
    {
        // increasing values for powers of 2
        for(unsigned int i = 0; i < arrays.size(); i++)
        {
            for(int j = 1; j <= pow(2, i); j++)
            {
                arrays[i].push_back(j);
            }
        }
    }
    else if(ARRAY_TYPE == 1)
    {
        // increasing values for powers of 2
        for(unsigned int i = 1; i < arrays.size(); i++)
        {
            if(i == 0)
            {
                arrays[i].push_back(1);
            }
            for(int j = 1; j < pow(2, i); j++)
            {
                arrays[i].push_back(j);
            }
        }
    }
    else if(ARRAY_TYPE == 2)
    {
        // decreasing values for powers of 2
        for(unsigned int i = 0; i < arrays.size(); i++)
        {
            for(int j = pow(2, i); j >= 1; j--)
            {
                arrays[i].push_back(j);
            }
        }
    }
    else if(ARRAY_TYPE == 3)
    {
        // decreasing values for powers of 2
        for(unsigned int i = 0; i < arrays.size(); i++)
        {
            for(int j = pow(2, i) - 1; j >= 1; j--)
            {
                arrays[i].push_back(j);
            }
        }
    }

    return arrays;
}

// checks to make sure that the vector is sorted
void is_sorted(const std::vector<int>& array)
{
    if(array.size() >= 2)
    {
        // check for increasing sort
        if(array[0] <= array[1])
        {
            for(unsigned int i = 0; i < array.size() - 1; i++)
            {
                if(array[i] > array[i + 1])
                {
                    throw std::runtime_error("Error: Not Sorted.");
                }
            }
        }
        // check for decreasing sort
        if(array[0] >= array[1])
        {
            for(unsigned int i = 0; i < array.size() - 1; i++)
            {
                if(array[i] < array[i + 1])
                {
                    throw std::runtime_error("Error: Not Sorted.");
                }
            }
        }
    }
}

// performs a binary search upon a vector while looking for a certain value
int binary_search(const std::vector<int>& array, int x)
{
    is_sorted(array);

    int mid = 0;
    int low = 0;
    int high = (int)(array.size() - 1);
    int num_comp = 0;

    while(low < high)
    {
        mid = (low + high) / 2;

        if(ARRAY_TYPE == 0 || ARRAY_TYPE == 1)
        {
            if(num_comp++, array[mid] < x) // < for increasing, > for decreasing
            {
                low = mid + 1;
            }
            else
            {
                high = mid;
            }
        }
        else if(ARRAY_TYPE == 2 || ARRAY_TYPE == 3)
        {
            if(num_comp++, array[mid] > x) // < for increasing, > for decreasing
            {
                low = mid + 1;
            }
            else
            {
                high = mid;
            }
        }
    }

    if(num_comp++, x == array[low])
    {
        std::cout << num_comp << std::endl;
        return low;
    }

    // throw an error is the vector isn't sorted
    throw std::runtime_error("Element does not exist");
}

// runs the program
int main(int argc, char* argv[])
{
    try
    {
        // creates the vectors
        std::vector<std::vector<int>> arrays = create_vectors();

        int x = 0;
        for(unsigned int i = 0; i < arrays.size(); i++)
        {
            if(arrays[i].size() > 0)
            {
                // sets the target number
                if(ARRAY_TYPE == 1 && i != 0)
                {
                    x = pow(2, i) - 1;
                    std::cout << "Finding " << x << " in 1 to " << x << ": ";
                }
                else if(ARRAY_TYPE == 0 || ARRAY_TYPE == 1)
                {
                    x = pow(2, i);
                    std::cout << "Finding " << x << " in 1 to " << x << ": ";
                }
                else if(ARRAY_TYPE == 2 || ARRAY_TYPE == 3)
                {
                    x = 1;
                    int y = 0;
                    if(ARRAY_TYPE == 3)
                    {
                        y = 1;
                    }
                    std::cout << "Finding 1 in " << pow(2, i) - y << " to 1: ";
                }
                binary_search(arrays[i], x); // perform the search
            }
        }

        return 0;
    }
    catch(std::runtime_error& e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
