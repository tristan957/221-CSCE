#include <iostream>

#include "my_string.hpp"

int main(int argc, char* argv[])
{
    my_string str1 = my_string();
    my_string str2 = my_string(5);
    my_string str3 = my_string("hello"); // resize
    my_string str4 = my_string("gg"); // resize
    std::cout << str3 << '\n';
    str3.insert(4, str4);
    std::cin >> str1;
    std::cout << str1 << std::endl;

    return 0;
}
