#ifndef MY_STRING_HPP
#define MY_STRING_HPP

#include <iostream>

class my_string
{
private:
    // Members
    char* ptr;
    int sz;
    int cap;

    // Private Functions
    int length(const char* word) const;
    void resize(int size);
    bool size_check(int i) const;

public:
    // Constructors
    my_string(void);
    explicit my_string(int size);
    explicit my_string(const char* word);
    my_string(const my_string& word);
    ~my_string();

    // Public Functions
    int size(void) const;
    int capacity(void) const;
    bool empty(void) const;
    my_string& insert(int pos, const my_string& s);
    char& at(int i) const;

    // Operators
    char& operator [] (int i) const;
    my_string& operator += (const char c);
    my_string& operator += (const my_string& word);
    my_string& operator = (const my_string& word);
    friend std::istream& operator >> (std::istream& in, my_string& word);
    friend std::ostream& operator << (std::ostream& out, my_string& word);
};

#endif
