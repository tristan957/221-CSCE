#include <iostream>
#include <stdexcept>

#include "my_string.hpp"

my_string::my_string(void)
{
    ptr = nullptr;
    sz = 0;
    cap = 0;
}

my_string::my_string(int size)
{
    ptr = new char[size];
    sz = 0;
    cap = sz;
}

my_string::my_string(const char* word)
{
    sz = length(word);
    ptr = new char[sz];
    for(int i = 0; i < sz; i++)
    {
        ptr[i] = word[i];
    }
    cap = sz;
}

my_string::my_string(const my_string& word)
{
    ptr = new char[word.capacity()];
    sz = word.size();
    cap = word.capacity();

    for(int i = 0; i < word.size(); i++)
    {
        ptr[i] = word[i];
    }
}

my_string::~my_string()
{
    if(ptr != nullptr)
    {
        delete[] ptr;
        ptr = nullptr;
    }
}

int my_string::length(const char* word) const
{
    int i = 0;
    while(word[i] != '\0')
    {
        i++;
    }
    return i;
}

// increase the storage capacity of a string
void my_string::resize(int size)
{
    if(size >= cap)
    {
        my_string temp = *this;

        delete[] ptr;

        cap = 2 * (size + 1); // double the size
        ptr = new char[cap];

        for(int i = 0; i < temp.size(); i++)
        {
            ptr[i] = temp[i];
        }
    }
}

bool my_string::size_check(int i) const
{
    if(i >= 0 && i < sz)
    {
        return true;
    }
    return false;
}

int my_string::size() const
{
    return sz;
}

int my_string::capacity() const
{
    return cap;
}

bool my_string::empty() const
{
    return !sz;
}

my_string& my_string::insert(int pos, const my_string& s)
{
    if(pos >= 0 && pos <= sz)
    {
        my_string tmp;
        for(int i = pos; i < sz; i++) // create temp variable
        {
            tmp += ptr[i];
        }

        resize(sz += s.size()); // resize ptr to include both char*

        for(int i = 0; i < tmp.size(); i++) // begin to populate ptr using new char
        {
            ptr[pos + i] = s[i];
        }

        for(int i = 0; i < tmp.size(); i++)
        {
            ptr[pos + s.size() + i] = tmp[i]; // finish populating ptr with the final characters
        }

        return *this;
    }
    else
    {
        throw std::runtime_error("Error: Index out of bounds.1\n");
    }
}

char& my_string::at(int i) const
{
    if(size_check(i) == true)
    {
        return ptr[i];
    }
    throw std::runtime_error("Error: Index out of bounds.2\n");
}

char& my_string::operator [] (int i) const
{
    return ptr[i];
}

my_string& my_string::operator += (const my_string& word)
{
    resize(sz + word.size());
    for(int i = 0; i < word.size(); i++)
    {
        ptr[sz] = word[i];
        sz++;
    }
    return *this;
}

my_string& my_string::operator += (const char c)
{
    resize(sz + 1);
    ptr[sz] = c;
    sz++;
    return *this;
}

my_string& my_string::operator = (const my_string& word)
{
    if(this != &word)
    {
        if(word.capacity() > cap)
        {
            delete[] ptr;
            ptr = new char[word.capacity()];
        }
        sz = word.size();
        cap = word.capacity();

        for(int i = 0; i < sz; i++)
        {
            ptr[i] = word[i];
        }
    }
    return *this;
}

std::istream& operator >> (std::istream& in, my_string& word)
{
    int c = 0;
    bool space = true;
    while(c = in.get())
    {
        if(c == '\n' || c == ' ')
        {
            if(space == true)   // check if the only character is a space or a newline
            {
                word.resize(word.sz + 1);
                word.ptr[word.sz] = ' ';
                word.sz++;
            }
            break;
        }
        if(space == true) {space = false;}
        word.resize(word.sz + 1);
        word.ptr[word.sz] = c;
        word.sz++;
    }
    return in;
}

std::ostream& operator << (std::ostream& out, my_string& word)
{
    for(int i = 0; i < word.sz; i++)
    {
        out << word[i];
    }
    return out;
}
