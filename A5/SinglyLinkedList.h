#ifndef SINGLY_LINKED_LIST_H
#define SINGLY_LINKED_LIST_H

#include <iostream>

template<class T>
class LinkedList;

template<class T>
class Node
{
    private:
        friend class LinkedList<T>;
        Node<T>* next;
        T elem;

    public:
        Node()
        {
            next = nullptr;
            elem = 0;
        }
        Node(T elem)
        {
            next = nullptr;
            this->elem = elem;
        }
        ~Node()
        {
            delete next;
            next = nullptr;
        }
        Node<T>* get_next()
        {
            return next;
        }
        T& get_elem()
        {
            return elem;
        }
};

template<class T>
class LinkedList
{
    private:
        int length;
        Node<T>* front;
        Node<T>* rear;

    public:
        LinkedList()
        {
            // front = nullptr;
            // rear = nullptr;
            length = 0;
            front = rear = nullptr;
        }

        ~LinkedList()
        {
            Node<T>* temp;
            while (front != nullptr)
            {
                temp = front;
                front = front->next;
                delete temp;
            }
            rear = nullptr;
        }

        Node<T>* get_front() const
        {
            return front;
        }

        Node<T>* get_rear() const
        {
            return rear;
        }

        void insert_front(T elem)
        {
            Node<T>* temp = new Node<T>(elem);

            if(front == nullptr)
            {
                rear = temp;
            }
            temp->next = front;
            front = temp;

            length++;
        }

        void insert_rear(T elem)
        {
            Node<T>* temp = new Node<T>(elem);

            if(front == nullptr)
            {
                front = rear = temp;
            }
            else
            { 
                rear->next = temp;
                rear = temp;
            }

            length++;
        }

        int size()
        {
            return length;
        }
        
        Node<T>* operator [] (int index)
        {
            Node<T>* temp = front;
            int i = 0;
            while(i != index)
            {
                temp = temp->next;
                i++;
            }

            return temp;
        }
        // friend std::ostream& operator << (std::ostream& out, const LinkedList<T>& list);
};

template<class T>
std::ostream& operator << (std::ostream& out, const LinkedList<T>& list)
{
    // std::cout << "hello\n";
    Node<T>* temp = list.get_front();
    while(temp != nullptr)
    {
        out << temp->get_elem() << ' ';
        temp = temp->get_next();
    }
    return out;
}

#endif