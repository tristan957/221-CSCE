#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include <iostream>
#include <typeinfo>
#include <vector>

#include "Person.h"
#include "SinglyLinkedList.h"

class HashTable
{
    private:
        std::vector<LinkedList<Person>> list;
        int length;
        unsigned int hash(const unsigned int key); // other functions call this. just does modulus
    
    public:
        HashTable();
        HashTable(int size);
        // ~HashTable();

        int size();
        std::vector<LinkedList<Person>> get_vec();
        void insert(const unsigned int key, void* value);
        void* search(const unsigned int key);

        LinkedList<Person>& operator [] (int index);
};

std::ostream& operator << (std::ostream& out, HashTable& htable);

#endif