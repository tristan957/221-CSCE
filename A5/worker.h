#include <fstream>
#include <iostream>
#include <string>
#include <regex>
#include <vector>

#include "HashTable.h"

void populate_table(std::string file_name, HashTable& htable);
void create_output(std::string file_name, HashTable& htable);
// std::vector<Person> parse(std::string content);