#ifndef PERSON_H
#define PERSON_H 

#include <iostream>
#include <string>

class Person
{
	private:
		std::string name;
		std::string email;
		unsigned int uin;
		std::string score;

	public:
		Person();
		Person(std::string name, std::string email, unsigned int uin);
		Person(std::string name, std::string email, unsigned int uin, std::string score);

		const std::string& get_name() const;
		const std::string& get_email() const;
		const unsigned int& get_uin() const;
		const std::string& get_score() const;

		void set_name(std::string name);
		void set_email(std::string email);
		void set_uin(unsigned long uin);
		void set_score(std::string score);

		friend std::ostream& operator << (std::ostream& out, const Person& peep);
};

#endif