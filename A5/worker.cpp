#include "worker.h"

void populate_table(std::string file_name,  HashTable& htable)
{
    // std::ifstream ifile(file_name);
	// std::string content((std::istreambuf_iterator<char>(ifile)),
    //                    (std::istreambuf_iterator<char>()));

    std::ifstream ifile = std::ifstream(file_name);
    std::string line;
    std::regex pattern = std::regex(R"(([\s\S]+),([\s\S]+),(\d{9}),(\d+))");
    std::smatch match;
    Person peep;
    while(ifile.good())
    {
        getline(ifile, line);
        regex_search(line, match, pattern);

        peep = Person(match[1].str(), match[2].str(), strtoul(match[3].str().c_str(), nullptr, 0), match[4].str());
        // std::cout << peep << '\n';
        htable.insert(peep.get_uin(), (void*)(&peep));
    }
    // return content;
}

void create_output(std::string file_name, HashTable& htable)
{
    std::ofstream ofile = std::ofstream("output.csv");
    std::ifstream ifile = std::ifstream(file_name);
    std::string line;
    std::regex pattern = std::regex(R"(([\s\S]+),([\s\S]+),(\d{9}))");
    std::smatch match;
    Person peep;
    Person* found_peep;
    while(ifile.good())
    {
        getline(ifile, line);
        regex_search(line, match, pattern);

        peep = Person(match[1].str(), match[2].str(), strtoul(match[3].str().c_str(), nullptr, 0));
        found_peep = (Person*)(htable.search(peep.get_uin()));
        ofile << peep.get_name() << ',' << peep.get_email() << ',' << peep.get_uin();
        if(found_peep != nullptr)
        {
            ofile << ',' << found_peep->get_score();
        }
        ofile << '\n';
    }
    ofile.close();
}

// std::vector<string> parse(std::string content)
// (([\s\S]+),([\s\S]+),(\d{9}),(\d+))\g