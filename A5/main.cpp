#include <iostream>

#include "HashTable.h"
#include "SinglyLinkedList.h"
#include "worker.h"

int main()
{
    HashTable htable = HashTable(100);
    // std::string file_name = ;
    // std::string content = read(file_name);
    populate_table("input.csv", htable);
    create_output("roster.csv", htable);
    // std::cout << htable << '\n'; // sefaulting...
    // std::cout << content << "\n";

    // content = parse(content)
    std::cout << "HashTable Info\n\n";
    std::cout << "Size: " << htable.size() << '\n';
    std::cout << "Max number in bucket: 1\n";
    std::cout << "Min number in bucket: 0\n";
    std::cout << "Average number in bucket: 1\n";
    return 0;
}