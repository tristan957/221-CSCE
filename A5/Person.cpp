#include "Person.h"

Person::Person()
{
	name = "";
	email = "";
	uin = 0;
	score = "";
}

Person::Person(std::string name, std::string email, unsigned int uin)
{
	this->name = name;
	this->email = email;
	this->uin = uin;
	score = "";
}

Person::Person(std::string name, std::string email, unsigned int uin, std::string score)
{
	this->name = name;
	this->email = email;
	this->uin = uin;
	this->score = score;
}

const std::string& Person::get_name() const
{
	return name;
}

const std::string& Person::get_email() const
{
	return email;
}

const unsigned int& Person::get_uin() const
{
	return uin;
}

const std::string& Person::get_score() const
{
	return score;
}

void Person::set_name(std::string name)
{
	this->name = name;
}

void Person::set_email(std::string email)
{
	this->email = email;
}

void Person::set_uin(unsigned long uin)
{
	this->uin = uin;
}

void Person::set_score(std::string score)
{
	this->score = score;
}

std::ostream& operator << (std::ostream& out, const Person& peep)
{
	return out << peep.get_name() << '(' << peep.get_uin() << ')' << ": " << peep.get_score();
}