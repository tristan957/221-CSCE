#include "HashTable.h"

HashTable::HashTable()
{
	length = 0;
	list = std::vector<LinkedList<Person>>();
}

HashTable::HashTable(int size)
{
	length = 0;
	list = std::vector<LinkedList<Person>>(size);
}

// HashTable::~HashTable()
// {
// 	// for(auto i : list) call destructor for every linked list
// 	// {
// 	// 	~i
// 	// }
// }

unsigned int HashTable::hash(const unsigned int key)
{
	return key % list.size();
}

int HashTable::size()
{
	return length;
}

std::vector<LinkedList<Person>> HashTable::get_vec()
{
	return list;
}

void HashTable::insert(const unsigned int key, void* value)
{
	list[hash(key)].insert_rear(*(Person*)(value));
	length++;
}

void* HashTable::search(const unsigned int key)
{
	unsigned int index = hash(key);
	int i = 0;
	while(i < list[index].size())
	{
		if(key == list[index][i]->get_elem().get_uin())
		{
			return (void*)(&list[index][i]->get_elem());
		}
	}

	return nullptr;
}

LinkedList<Person>& HashTable::operator [] (int index)
{
	return list[index];
}

std::ostream& operator << (std::ostream& out, HashTable& htable)
{
	std::vector<LinkedList<Person>> list = htable.get_vec();
	std::cout << htable.size() << '\n';
	for(int i = 0; i < htable.size(); i++)
	{
		std::cout << list[i].size() << '\n';
		for(int j = 0; j < list[i].size(); j++)
		{
			out << list[i][j]->get_elem() << '\n';
		}
	}
	
	return out;
}