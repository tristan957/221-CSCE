// LinkedQueue.h

#ifndef LINKEDQUEUE_H
#define LINKEDQUEUE_H

#include <iostream>
#include "RuntimeException.h"
#include "TemplateDoublyLinkedList.h"

template<typename T> class LinkedQueue;

template<typename T>
std::ostream& operator<<(std::ostream& out, const LinkedQueue<T>& queue);

template<typename T>
class LinkedQueue
{
private:
   /* declare member variables here */
    DoublyLinkedList<T> dll;
public:
   // user-defined exceptions
   class QueueEmptyException : public RuntimeException {
   public:
     QueueEmptyException() : RuntimeException("Access to an empty queue") {}
   };

   /* declare rest of functions */
    LinkedQueue<T>() : dll() { }
    LinkedQueue<T>(const LinkedQueue<T>& lq);
    // O(n)
    ~LinkedQueue<T>() { dll.~DoublyLinkedList(); }
    T first() const throw(QueueEmptyException);
    bool isEmpty() const throw(QueueEmptyException) { return dll.isEmpty(); }
    // O(1)
    void enqueue(T elem) { dll. insertLast(elem); } // adds elements to the queue
    T dequeue() throw(QueueEmptyException);
    const DoublyLinkedList<T> getDLL() const;
};

/* describe rest of the functions here */

// copy constructor
// O(1)
template <typename T>
LinkedQueue<T>::LinkedQueue(const LinkedQueue<T>& lq)
{
    dll = lq.getDLL();
}

// peek at the top of the queue
// O(1)
template <typename T>
T LinkedQueue<T>::first() const throw(QueueEmptyException)
{
    if (dll.isEmpty())
        throw QueueEmptyException();
    return dll.first();
}

// dequeues elements
// O(1)
template <typename T>
T LinkedQueue<T>::dequeue() throw(QueueEmptyException)
{
    if (dll.isEmpty())
        throw QueueEmptyException();
    return dll.removeFirst();
}

// returns the DoublyLinkedList
// O(1)
template <typename T>
const DoublyLinkedList<T> LinkedQueue<T>::getDLL() const
{
    return dll;
}

// O(n)
template<typename T>
std::ostream& operator<<(std::ostream& out, const LinkedQueue<T>& queue)
{
  /* fidll in the function */
    DoublyLinkedList<T> dll = queue.getDLL();
    DListNode<T>* node = dll.getFirst();
    while(node != dll.getAfterLast()->getPrev())
    {
        out << node->getElem().kind;
        node = node->getNext();
    }
    return out;
}

#endif
