#ifndef TEMPLATED_DOUBLY_LINKED_LIST_H
#define TEMPLATED_DOUBLY_LINKED_LIST_H

#include <cstdlib>
#include <iostream>
#include <stdexcept>

using namespace std;

// extend range_error from <stdexcept>
struct EmptyDLinkedListException : std::range_error {
	explicit EmptyDLinkedListException(char const* msg=NULL): range_error(msg) {}
};

// class instantiation
template<class T> class DoublyLinkedList;

// node for a doubly linked list
template<class T> class DListNode {
private:
  T obj;
  DListNode<T> *prev, *next;
  friend class DoublyLinkedList<T>;
public:
  DListNode<T>(T e=T(), DListNode<T> *p = nullptr, DListNode<T> *n = nullptr)
	: obj(e), prev(p), next(n) {}
  T getElem() const { return obj; } // O(1)
  DListNode<T> * getNext() const { return next; } // O(1)
  DListNode<T> * getPrev() const { return prev; } // O(1)
};

// doubly linked list
template<class T> class DoublyLinkedList {
protected:
  DListNode<T> header, trailer;
public:
  DoublyLinkedList<T>() : // constructor
	header(T()), trailer(T())
  { header.next = &trailer; trailer.prev = &header; }
  DoublyLinkedList<T>(const DoublyLinkedList<T>& dll); // copy constructor
  ~DoublyLinkedList<T>(); // destructor
  DoublyLinkedList<T>& operator=(const DoublyLinkedList<T>& dll); // assignment operator
  // return the pointer to the first node
  DListNode<T> *getFirst() const { return header.next; }
  // return the pointer to the trailer
  const DListNode<T> *getAfterLast() const { return &trailer; }
  // check if the list is empty
  bool isEmpty() const { return header.next == &trailer; } // O(1)
  T first() const; // return the first object
  T last() const; // return the last object
  void insertFirst(T newobj); // insert to the first of the list
  T removeFirst(); // remove the first node
  void insertLast(T newobj); // insert to the last of the list
  T removeLast(); // remove the last node
};

// copy constructor
// O(n)
template<class T> DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<T>& dll)
{
	// Initialize the list
	header.next = &trailer;
	trailer.prev = &header;

	DListNode<T>* temp = dll.getFirst();
	while(temp != dll.getAfterLast())
	{
		insertLast(temp->getElem());
		temp = temp->getNext();
	}
}

// assignment operator
// O(n)
template<class T> DoublyLinkedList<T>& DoublyLinkedList<T>::operator = (const DoublyLinkedList<T>& dll)
{
	if(this != &dll)
	{
		header.next = &trailer;
		trailer.prev = &header;

		DListNode<T>* temp = dll.getFirst();
		while(temp != dll.getAfterLast())
		{
			insertLast(temp->getElem());
			temp = temp->getNext();
		}
	}
	return *this;
}

// insert the object to the first of the linked list
// O(1)
template<class T> void DoublyLinkedList<T>::insertFirst(T newobj)
{
	DListNode<T> *newNode = new DListNode<T>(newobj, &header, header.next);
	DListNode<T>* ptr = header.next;
	header.next = newNode;
	newNode->next = ptr;
}

// insert the object to the last of the linked list
// O(1)
template<class T> void DoublyLinkedList<T>::insertLast(T newobj)
{
	DListNode<T> *newNode = new DListNode<T>(newobj, trailer.prev, &trailer);
	trailer.prev->next = newNode;
	trailer.prev = newNode;
}

// remove the first object of the list
// O(1)
template<class T> T DoublyLinkedList<T>::removeFirst()
{
	if (isEmpty())
		throw EmptyDLinkedListException("Empty Doubly Linked List");
	DListNode<T>* node = header.next;
	node->next->prev = &header;
	header.next = node->next;
	T obj = node->obj;
	delete node;
	return obj;
}

// remove the last object of the list
// O(1)
template<class T> T DoublyLinkedList<T>::removeLast()
{
	if (isEmpty())
		throw EmptyDLinkedListException("Empty Doubly Linked List");
	DListNode<T> *node = trailer.prev;
	node->prev->next = &trailer;
	trailer.prev = node->prev;
	T obj = node->obj;
	delete node;
	return obj;
}

// destructor
// O(n)
template<class T> DoublyLinkedList<T>::~DoublyLinkedList()
{
	DListNode<T> *prev_node, *node = header.next;
	while (node != &trailer)
	{
		prev_node = node;
		node = node->next;
		delete prev_node;
	}
	header.next = &trailer;
	trailer.prev = &header;
}

// return the first object
// O(1)
template<class T> T DoublyLinkedList<T>::first() const
{
	if (isEmpty())
		throw EmptyDLinkedListException("Empty Doubly Linked List");
	return header.next->getElem();
}

// return the last object
// O(1)
template<class T> T DoublyLinkedList<T>::last() const
{
	if (isEmpty())
		throw EmptyDLinkedListException("Empty Doubly Linked List");
	return trailer.getElem();
}

// output operator
// O(n)
template<class T> ostream& operator << (ostream& out, const DoublyLinkedList<T>& dll)
{
	DListNode<T>* node = dll.getFirst();
	while(node != dll.getAfterLast())
	{
		out << node->getElem() << " ";
		node = node->getNext();
	}
	return out;
}

#endif
