#ifndef PARSER_H
#define PARSER_H

#include "LinkedStack.h"
#include "LinkedQueue.h"
#include "RuntimeException.h"
#include <vector>


struct Token
{
    char kind;
    double value;
    int weight;

    Token() : kind(), value(0), weight(get_operator_weight()) {}
    Token(char ch) : kind(ch), value(0), weight(get_operator_weight()) {}
    Token(char ch, int lb) : kind(ch), value(0), weight(lb) {}

    bool isOperator()
    {
        return kind == '+' || kind == '-' || kind == '*' || kind == '/' || kind == '^';
    }
    bool isOperand()
    {
        return isNum() || isVar();
    }
    bool isNum()
    {
        return kind >= '0' && kind <= '9' || kind == '~';
    }
    bool isVar()
    {
        return kind >= 'a' && kind <= 'z';
    }
    void set_value(std::string word)
    {
        const char* c_word = word.c_str();
        value = atof(c_word);
    }
    void char_to_double()
    {
        value = (double)(kind - 48);
    }
    int get_operator_weight()
    {
        int weight = 0;
        switch(kind)
        {
            case '(':
            case ')':
                weight = 1;
                break;
            case '+':
            case '-':
                weight = 2;
                break;
            case '*':
            case '/':
                weight = 3;
                break;
            case '^':
                weight = 4;
                break;
            case '~':
                weight = 5;
                break;
        }
        return weight;
    }
};


class Parser {
private:
    /* declare constants */
    Token MULT;
    Token DIV;
    Token ADD;
    Token SUB;
    Token OPAR;
    Token CPAR;

    /* declare member variables;
    may include a string postfix queue and a integer operator stack */
    std::string infix;
    std::vector<Token> tokenList;
    LinkedStack<Token> opStack;
    LinkedQueue<Token> postfix;

    /* declare utility functions */
    LinkedStack<std::string> stringToStack(std::string s);
    LinkedQueue<Token> toPostfix();
    int operatorWeight(char c);
    void string_2_vec();

public:
    // constructor
    Parser(void)
    {
        infix = "";
    }
    Parser(std::string s) : infix(s) {
        MULT = Token('*', 3);
        DIV  = Token('/', 3);
        ADD  = Token('+', 2);
        SUB  = Token('-', 2);
        OPAR = Token('(', 1);
        CPAR = Token(')', 1);
        opStack.push(Token('#'));
        string_2_vec();
        toPostfix();
    }

    // accessor method
    LinkedQueue<Token> getPostfix() { return postfix; }

    // operations
    void printInfix() { std::cout << infix << '\n'; } // O(1)
    void printPostfix() { std::cout << postfix << '\n'; } // O(n)
};

#endif
