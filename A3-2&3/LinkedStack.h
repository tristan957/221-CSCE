// LinkedStack.h

#ifndef LINKEDSTACK_H_
#define LINKEDSTACK_H_

#include <iostream>
#include "RuntimeException.h"
#include "TemplateDoublyLinkedList.h"

template<typename T> class LinkedStack;

template<typename T>
std::ostream& operator<<(std::ostream& out, const LinkedStack<T>& stack);

template<typename T>
class LinkedStack
{
private:
    /* declare member variables here */
    DoublyLinkedList<T> dll;

public:
   // user-defined exceptions
   class StackEmptyException : public RuntimeException {
   public:
     StackEmptyException() : RuntimeException("Stack is empty") {}
   };

    /* declare rest of functions */
    LinkedStack<T>() : dll() { }
    // O(n)
    ~LinkedStack<T>() { dll.~DoublyLinkedList(); }
    // O(1)
    bool isEmpty() const { return dll.isEmpty(); }
    // O(1)
    void push(const T elem) { dll.insertFirst(elem); }
    T pop() throw(StackEmptyException);
    T top() const throw (StackEmptyException);
    const DoublyLinkedList<T> getDLL() const;
};

/* describe rest of the functions here */
// O(1)
template <typename T>
T LinkedStack<T>::pop() throw(StackEmptyException)
{
    if (dll.isEmpty())
        throw StackEmptyException();
    return dll.removeFirst();
}

template <typename T>
// O(1)
T LinkedStack<T>::top() const throw(StackEmptyException)
{
    if (dll.isEmpty())
        throw StackEmptyException();
    return dll.first();
}

// O(1)
template <typename T>
const DoublyLinkedList<T> LinkedStack<T>::getDLL() const
{
    return dll;
}

// O(n)
template<typename T>
std::ostream& operator<<(std::ostream& out, const LinkedStack<T>& stack)
{
  /* fill in the function */
   return out << stack.getDLL();
}

#endif
