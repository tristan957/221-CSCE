#include "Parser.h"

/* describe rest of functions */
// converts an infix to postfix
// O(n)
LinkedQueue<Token> Parser::toPostfix()
{
    Token item;
    for (int i = 0; i < tokenList.size(); i++)
    {
        item = tokenList[i];
        if(!(opStack.isEmpty()) && item.isOperand())
        {
            do
            {
                postfix.enqueue(item);
                if(i < tokenList.size())
                    item = tokenList[++i];
                if(i == tokenList.size() + 1)
                    break;
            } while(!(opStack.isEmpty()) && item.isOperand());
            postfix.enqueue(Token(' '));
            i--;
        }
        else if(!(opStack.isEmpty()) && item.kind == ')')
        {
            Token topToken = opStack.pop();
            while(topToken.kind != '(')
            {
                postfix.enqueue(topToken);
                topToken = opStack.pop();
            }
        }
        else if(item.kind == '#')
        {
            while (!(opStack.isEmpty()))
                postfix.enqueue(opStack.pop());
        }
        else if(!(opStack.isEmpty()) && item.kind == '(')
            opStack.push(item);
        else
        {
            while(!(opStack.isEmpty()) && opStack.top().weight >= item.weight)
                postfix.enqueue(opStack.pop());
            postfix.enqueue(Token(' '));
            opStack.push(item);
        }
    }
    while(!(opStack.isEmpty()))
    {
        postfix.enqueue(opStack.pop());
    }
    return postfix;
}

// converts a string to a vector
// O(n)
void Parser::string_2_vec()
{
    Token t;
    for(int i = 0; i < infix.size(); i++)
    {
        t = Token(infix[i]);
        if(t.isVar())
        {
            std::cout << "Enter value for " << t.kind << ": ";
            std::string word;
            std:cin >> word;
            t.set_value(word);
        }
        if(t.isNum())
        {
            t.char_to_double();
        }
        tokenList.push_back(t);
    }
}
