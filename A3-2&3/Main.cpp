#include "RuntimeException.h"
#include "Parser.h"
#include "Evaluator.h"

// create a menu and getuser choice
int menu()
{
    std::cout << "1. Read an infix expression from the keyboard\n";
    std::cout << "2. Check whether or not parenthesis are balanced correctly\n";
    std::cout << "3. Display a correct infix expression on the screen or a message that the expression is invalid\n";
    std::cout << "4. Convert infix form to its postfix form and display a postfix queue on the screen\n";
    std::cout << "5. Evaluate postfix form of the expression for floating point values entered from the keyboard\n";
    std::cout << "6. Display the value of an algebraic expression on the screen\n";
    std::cout << "7. Quit\n";
    std::cout << "Enter a choice: ";
    int choice;
    std::cin >> choice;
    return choice;
}

// checks if parenthesis are balanced
// O(n)
bool check_paren(std::string s)
{
    int o = 0;
    int c = 0;
    for(unsigned int i = 0; i < s.size(); i++)
    {
        if(s[i] == '(')
            o++;
        if(s[i] == ')')
            c++;
    }
    return o == c;
}

int main()
{
    Parser par;
    std::string expr = "";
    double value = 0;
    bool w = true;
    while(w)
    {
        int choice = menu();
        switch(choice)
        {
            case 1:
            {
                std::cout << "Enter an infix expression: ";
                std::cin >> expr;
                break;
            }
            case 2:
            {
                if(expr != "")
                {
                    if(check_paren(expr))
                        std::cout << "The parenthesis match.\n";
                    else
                        std::cout << "The parenthesis don't match.\n";
                }
                else
                    std::cout << "No expression entered.\n";
                break;
            }
            case 3:
            {
                if(expr != "")
                {
                    if(check_paren(expr))
                        std::cout << "Here is the correct infix expression: " << expr << std::endl;
                    else
                        std::cout << "Your infix expression is invalid.\n";
                }
                else
                    std::cout << "No expression entered.\n";
                break;
            }
            case 4:
            {
                if(expr != "")
                {
                    par = Parser(expr);
                    std::cout << "The postfix is: " << par.getPostfix() << std::endl;
                }
                else
                    std::cout << "No expression entered.\n";
                break;
            }
            case 5:
            {
                if(expr != "")
                {
                    Evaluator eval = Evaluator(par);
                    value = eval.getValue();
                    std::cout << "A value has been calculated.\n";
                }
                else
                    std::cout << "No postfix expression created.\n";
                break;
            }
            case 6:
            {
                if(expr != "")
                {
                    std::cout << "The value of your expression is: " << value << std::endl;
                }
                else
                    std::cout << "No postfix expression entered.\n";
                break;
            }
            case 7:
            {
                w = false;
                break;
            }
            default:
                std::cout << "Error: Not valid choice.\n\n";
                break;
        }
        std::cout << std::endl;
    }
    return 0;
}
