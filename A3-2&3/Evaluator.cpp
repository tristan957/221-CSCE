#include "Evaluator.h"

using namespace std;

// O(1)
double Evaluator::evaluate(char op, double val1, double val2 )
{
    switch (op) {
        case '+':
            return val1 + val2;
        case '-':
            return val1 - val2;
        case '*':
            return val1 * val2;
        case '/':
            if(val2 == 0)
                throw DivisionByZeroException();
            return val1 / val2;
        case '^':
            return pow(val1, val2);
    }
}

// O(n)
double Evaluator::getValue()
{
    /* returns the result of expression evaluation */
    tokenQue = par.getPostfix();
    while(tokenQue.first().kind != '#')
    {
        while(tokenQue.first().isOperand())
        {
            double val;
            bool neg = false;
            stringstream ss;
            do
            {
                if(tokenQue.first().kind == '~')
                {
                    neg = true;
                    tokenQue.dequeue();
                }
                ss << tokenQue.dequeue().value;
            } while(tokenQue.first().kind != ' '); // tokenQue.first().kind != '#' &&
            ss >> val;
            if(neg == true)
                val*=-1;
            doubleStack.push(val);
        }
        if(tokenQue.first().kind != ' ' && tokenQue.first().isOperator())
            doubleStack.push(evaluate(tokenQue.dequeue().kind, doubleStack.pop(), doubleStack.pop()));
        else
            tokenQue.dequeue();
    }
    return doubleStack.pop();
}
