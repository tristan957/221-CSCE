// Tristan Partin
// t3104@tamu.edu
// 224009988
// 221-510

#include <iostream>
#include <string>
#include <regex>

// a. matches stores an array of strings that match the regex expression.
// b. \d means digit

int main()
{
	std::regex pattern1(R"(\d\d)");
	std::regex pattern2(R"(thanks)");
	std::string to_search = "I would like the number 98 to be found and printed, thanks.";

	std::smatch matches1;
	std::smatch matches2;

	regex_search(to_search, matches1, pattern1);
	regex_search(to_search, matches2, pattern2);

	for(auto item : matches1)
	{
		std::cout << item << std::endl;
	}
	for(auto item : matches2)
	{
		std::cout << item << std::endl;
	}
	return 0;
}
