// Tristan Partin
// t3104@tamu.edu
// 224009988
// 221-510

#include <iostream>
#include <string>
#include <regex>

// a. \s\S means that the characters before </title> can be either whitespace or not whitespace
// b. <title>This is a title</title>
// c. There are 2 regex patterns written. The [\s\S]+ in the parenthesis means to also grab the string in between the tags by creating a group.

int main()
{
	std::string to_search = "<html><head>Wow such a header <title>This is a title</title>So top</head>Much body</html>";

	std::regex pattern4{R"(<head>(.*?)<title>)"};
	std::regex pattern5{R"(</title>(.*?)</head>)"};
	std::smatch matches2;
	std::smatch matches3;

	regex_search(to_search, matches2, pattern4);
	regex_search(to_search, matches3, pattern5);

	std::cout << matches2[1] << std::endl;
	std::cout << matches3[1] << std::endl;

	return 0;
}
