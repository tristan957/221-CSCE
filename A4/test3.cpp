// Tristan Partin
// t3104@tamu.edu
// 224009988
// 221-510

#include <iostream>
#include <string>
#include <regex>
#include <fstream>

int main()
{
	std::ifstream ifile("stroustrup.txt");
	std::string content((std::istreambuf_iterator<char>(ifile)),
                       (std::istreambuf_iterator<char>()));

	std::regex pattern1{R"(<a href=\"(.*?.ppt)\")"};
	std::smatch matches1;

	std::string::const_iterator searchStart(content.cbegin());
	while(regex_search(searchStart, content.cend(), matches1, pattern1))
	{
		std::cout << ( searchStart == content.cbegin() ? "" : "\n" ) << matches1[1];
        searchStart += matches1.position() + matches1.length();
	}
	std::cout << std::endl;

	return 0;
}
