// programming assignment 3

#include "DoublyLinkedList.h"
#include <stdexcept>


// extend range_error from <stdexcept>
struct EmptyDLinkedListException : std::range_error {
    explicit EmptyDLinkedListException(char const* msg=NULL): range_error(msg) {}
};

// copy constructor
// O(n)
DoublyLinkedList::DoublyLinkedList(const DoublyLinkedList& dll)
{
    // Initialize the list
    header.next = &trailer;
    trailer.prev = &header;

    DListNode* temp = dll.getFirst();
    while(temp != dll.getAfterLast())
    {
        insertLast(temp->getElem());
        temp = temp->getNext();
    }
}

// assignment operator
// O(n)
DoublyLinkedList& DoublyLinkedList::operator = (const DoublyLinkedList& dll)
{
    if(this != &dll)
    {
        // header.next = &trailer;
        // trailer.prev = &header;

        DListNode* temp = dll.getFirst();
        while(temp != dll.getAfterLast())
        {
            insertLast(temp->getElem());
            temp = temp->getNext();
        }
    }
    return *this;
}

// insert the object to the first of the linked list
// O(1)
void DoublyLinkedList::insertFirst(int newobj)
{
	DListNode *newNode = new DListNode(newobj, &header, header.next);
	header.next->prev = newNode;
	header.next = newNode;
}

// insert the object to the last of the linked list
// O(1)
void DoublyLinkedList::insertLast(int newobj)
{
    DListNode *newNode = new DListNode(newobj, trailer.prev, &trailer);
    trailer.prev->next = newNode;
    trailer.prev = newNode;
}

// remove the first object of the list
// O(1)
int DoublyLinkedList::removeFirst()
{
    if (isEmpty())
        throw EmptyDLinkedListException("Empty Doubly Linked List");
    DListNode* node = header.next;
    node->next->prev = &header;
    header.next = node->next;
    int obj = node->obj;
    delete node;
    return obj;
}

// remove the last object of the list
// O(1)
int DoublyLinkedList::removeLast()
{
    if (isEmpty())
        throw EmptyDLinkedListException("Empty Doubly Linked List");
    DListNode *node = trailer.prev;
    node->prev->next = &trailer;
    trailer.prev = node->prev;
    int obj = node->obj;
    delete node;
    return obj;
}

// destructor
// O(n)
DoublyLinkedList::~DoublyLinkedList()
{
  	DListNode *prev_node, *node = header.next;
  	while (node != &trailer)
	{
		prev_node = node;
		node = node->next;
		delete prev_node;
  	}
  	header.next = &trailer;
  	trailer.prev = &header;
}

// return the first object
// O(1)
int DoublyLinkedList::first() const
{
  	if (isEmpty())
		throw EmptyDLinkedListException("Empty Doubly Linked List");
  	return header.getElem();
}

// return the last object
// O(1)
int DoublyLinkedList::last() const
{
	if (isEmpty())
		throw EmptyDLinkedListException("Empty Doubly Linked List");
	return trailer.getElem();
}

// output operator
// O(n)
ostream& operator << (ostream& out, const DoublyLinkedList& dll)
{
	DListNode* node = dll.getFirst();
  	while(node != dll.getAfterLast())
  	{
	  	out << node->getElem() << " ";
	  	node = node->getNext();
  	}
  	return out;
}
