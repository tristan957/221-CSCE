#include <iostream>
#include "DoublyLinkedList.h"

int main()
{
    DoublyLinkedList list;
    list.insertFirst(5);
    list.insertFirst(6);
    list.insertFirst(7);
    list.removeFirst();
    DoublyLinkedList list2 = list;
    std::cout << list2 << std::endl;
    return 0;
}
