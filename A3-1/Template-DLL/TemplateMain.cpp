// programming assignment 3

#include "TemplateDoublyLinkedList.h"
#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main () {
  // Construct a linked list with header & trailer
  cout << "Create a new list" << endl;
  DoublyLinkedList<string> dll;
  DoublyLinkedList<int> dll_int;
  cout << "list: " << dll << endl << endl;
  cout << "int list: " << dll_int << endl << endl;

  // Insert 10 nodes at back with value 10,20,30,..,100
  cout << "Insert 10 nodes at back with value 10,20,30,..,100" << endl;
  for (int i=10; i<=100; i+=10) {
    stringstream ss;
    ss << i;
    dll.insertLast(ss.str());
    dll_int.insertLast(i);
  }
  cout << "list: " << dll << endl << endl;
  cout << "int list: " << dll_int << endl << endl;

  // Insert 10 nodes at front with value 10,20,30,..,100
  cout << "Insert 10 nodes at front with value 10,20,30,..,100" << endl;
  for (int i=10; i<=100; i+=10) {
    stringstream ss;
    ss << i;
    dll.insertFirst(ss.str());
    dll_int.insertFirst(i);
  }
  cout << "list: " << dll << endl << endl;
  cout << "int list: " << dll_int << endl << endl;

  // Copy to a new list
  cout << "Copy to a new list" << endl;
  DoublyLinkedList<string> dll2(dll);
  DoublyLinkedList<int> dll_int2(dll_int);
  cout << "list2: " << dll2 << endl << endl;
  cout << "int list2: " << dll_int2 << endl << endl;

  // Assign to another new list
  cout << "Assign to another new list" << endl;
  DoublyLinkedList<string> dll3;
  dll3=dll;
  DoublyLinkedList<int> dll_int3;
  dll_int3=dll_int;
  cout << "list3: " << dll3 << endl << endl;
  cout << "int list3: " << dll_int3 << endl << endl;

  // Delete the last 10 nodes
  cout << "Delete the last 10 nodes" << endl;
  for (int i=0; i<10; i++) {
    dll.removeLast();
    dll_int.removeLast();
  }
  cout << "list: " << dll << endl << endl;
  cout << "int list: " << dll_int << endl << endl;

  // Delete the first 10 nodes
  cout << "Delete the first 10 nodes" << endl;
  for (int i=0; i<10; i++) {
    dll.removeFirst();
    dll_int.removeFirst();
  }
  cout << "list: " << dll << endl << endl;
  cout << "int list: " << dll_int << endl << endl;

  // Check the other two lists
  cout << "Make sure the other two lists are not affected." << endl;
  cout << "list2: " << dll2 << endl;
  cout << "list3: " << dll3 << endl;
  cout << "int list2: " << dll_int2 << endl;
  cout << "int list3: " << dll_int3 << endl;

  return 0;
}
