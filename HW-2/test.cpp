#include <iostream>

int maxArray(int ar[], int size)
{
	if (size == 1)
		return ar[0];
	else
	{
		int max = maxArray(ar, size - 1);
		return ar[size - 1] > max ? ar[size - 1] : max;
	}
}

int main()
{
	int swag[] = {-1, 2, 3, 4, 5};
	int max = maxArray(swag, 5);
	std::cout << max << std::endl;
}
